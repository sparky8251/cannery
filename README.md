Cannery aims to make it easier to develop containers with many different needs. It does this by operating off of a cannery.toml file in the docker dir.

In this cannery.toml file you define build rules, run rules, etc, and cannery takes care of the rest!
