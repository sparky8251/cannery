mod subcommands;

use structopt::StructOpt;

use subcommands::{build, clean, run, start, status, stop};

#[derive(Debug, StructOpt)]
#[structopt(name = "cannery", about = "A simpler way to develop containers")]
enum Opts {
    Build,
    Clean,
    Run,
    Start,
    Status,
    Stop,
}

fn main() {
    match Opts::from_args() {
        Opts::Build => build(),
        Opts::Clean => clean(),
        Opts::Run => run(),
        Opts::Start => start(),
        Opts::Status => status(),
        Opts::Stop => stop(),
    }
}
