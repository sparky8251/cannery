use config::{Config, File};
use std::process::Command;

pub fn clean() {
    let mut c = Config::new();
    c.merge(File::with_name(".cannery").required(true)).unwrap();
    let name = c.get_str("general.name").unwrap();
    let mut input: String = "rm ".to_owned();

    input += "-f ";
    input += &name;

    let input: &str = &input[..];
    let input: Vec<&str> = input.split_whitespace().collect();
    println!("{:?}", input);

    let output = Command::new("docker")
        .args(&input)
        .output()
        .expect("failed to execute process");

    println!(" Process complete. The output was: \n");
    println!("{}", String::from_utf8_lossy(&output.stdout));
    println!(" Process complete. If there where errors, they were: \n");
    println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
}
