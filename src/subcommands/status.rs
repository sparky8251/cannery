use std::process::Command;

pub fn status() {
    let input: String = "ps -a".to_owned();
    let input: &str = &input[..];
    let input: Vec<&str> = input.split_whitespace().collect();

    let output = Command::new("docker")
        .args(&input)
        .output()
        .expect("failed to execute process");

    println!("{}", String::from_utf8_lossy(&output.stdout));
}
