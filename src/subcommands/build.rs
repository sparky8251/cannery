use config::{Config, File};
use std::process::Command;

pub fn build() {
    let mut c = Config::new();
    c.merge(File::with_name(".cannery").required(true)).unwrap();
    let mode: &str = "dev/";
    let name = c.get_str("general.name").unwrap();
    let dockerfile = c.get_str("build.dockerfile").unwrap();
    let workingdir = c.get_str("build.workingdir").unwrap();

    let mut input: String = "build ".to_owned();

    input += "-t ";
    input += &mode;
    input += &name;
    input += " ";
    input += "-f ";
    input += &dockerfile;
    input += " ";
    input += &workingdir;

    let input: &str = &input[..];
    let input: Vec<&str> = input.split_whitespace().collect();

    let output = Command::new("docker")
        .args(&input)
        .output()
        .expect("failed to execute process");

    println!(" Process complete. The output was: \n");
    println!("{}", String::from_utf8_lossy(&output.stdout));
    println!(" Process complete. If there where errors, they were: \n");
    println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
}
