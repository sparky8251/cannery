use config::{Config, File};
use std::process::Command;

pub fn run() {
    let mut c = Config::new();
    c.merge(File::with_name(".cannery").required(true)).unwrap();
    let mode: &str = "dev/";
    let name = c.get_str("general.name").unwrap();
    let ports = c.get_str("development.ports").unwrap();
    let ports: &str = &ports[..];
    let ports: Vec<&str> = ports.split_whitespace().collect();
    let volumes = c.get_str("development.volumes").unwrap();
    let volumes: &str = &volumes[..];
    let volumes: Vec<&str> = volumes.split_whitespace().collect();

    let mut input: String = "run -d ".to_owned();
    for p in ports.iter() {
        input += "-p ";
        input += &p;
        input += " ";
    }
    for v in volumes.iter() {
        input += "-v ";
        input += &v;
        input += " ";
    }
    input += "--name ";
    input += &name;
    input += " ";
    input += &mode;
    input += &name;

    let input: &str = &input[..];
    let input: Vec<&str> = input.split_whitespace().collect();
    println!("{:?}", input);

    let output = Command::new("docker")
        .args(&input)
        .output()
        .expect("failed to execute process");

    println!(" Process complete. The output was: \n");
    println!("{}", String::from_utf8_lossy(&output.stdout));
    println!(" Process complete. If there where errors, they were: \n");
    println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
}
